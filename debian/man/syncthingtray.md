# syncthingtray(1) -- Tray application for Syncthing

## SYNOPSIS

`syncthingtray` [options]   
`syncthingtray` `--help`

## DESCRIPTION

Graphical user interface for Syncthing.

To see a full list of command line options run `syncthingtray --help`.

